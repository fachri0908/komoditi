@extends('Admin.layout')
@section('content')

    <div class="panel-heading">
        <h2>Data Pasar</h2>
    </div>
    <div class="panel-body">
      <h5><a href="{{url("/admin/pasar/new")}}" class="btn btn-primary">+Tambah Data Pasar</a></h5>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                          <tr>
                            <th>No</th>
                            <th>Nama Pasar (id)</th>
                            <th>Aksi</th>
                          </tr>
                          @foreach($pasar as $row)
                            <tr>
                              <td>{{$loop->iteration}}</td>
                              <td>{{$row->nama_pasar}} ({{$row->id}})</td>
                              <td>
                                <a href="{{url('admin/pasar/edit/' . $row->id)}}">Ubah</a> |
                                <a href="{{url('admin/pasar/delete/' . $row->id)}}" onclick="return confirm('Anda yakin ingin menghapus data ?')">Hapus</a>
                              </td>
                            </tr>
                          @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
