@extends('Admin.layout')
@section('content')
<div class="panel-heading">
  <h2>Data User</h2>
</div>
<div class="panel-body">
  <h5><a href="{{url("/admin/user/new")}}" class="btn btn-primary">+Tambah Data Admin</a></h5>
  <div class="row">
    <div class="col-lg-12">
      <table class="table table-bordered">
        <tr>
          <th>No</th>
          <th>Username</th>
          <th>Password</th>
          @if(Auth::user()->username=='dajoe')
          <th>Aksi</th>
          @endif
        </tr>
        @foreach($user as $row)
          <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$row->username}}</td>
            <td>***************</td>
            @if(Auth::user()->username=='dajoe')
            <td>
              <a href="{{url('admin/user/delete/' . $row->id)}}" onclick="return confirm('Anda yakin ingin menghapus data ?')">Hapus</a>
            </td>
            @endif
          </tr>
        @endforeach
      </table>
    </div>
  </div>
</div>
@endsection
