@extends('Admin.layout')
@section('content')
<div class="panel-heading">
  <h2>Data Komoditas</h2>
</div>
<div class="panel-body">
  <h5><a href="{{url("/admin/komoditi/new")}}" class="btn btn-primary">+Tambah Data Komoditi</a></h5>
  <div class="row">
    <div class="col-lg-12">
      <table class="table table-bordered">
        <tr>
          <th>ID</th>
          <th>Nama Komoditi</th>
          <th>Kategori</th>
          <th>Satuan</th>
          <th>Aksi</th>
        </tr>
        @foreach($komoditi as $row)
          <tr>
            <td>{{$row->id}}</td>
            <td>{{$row->nama_komoditi}}</td>
            <td>
              {{$row->kategori->nama_kategori}}
            </td>
            <td>
              {{$row->satuan->nama_satuan}}
            </td>
            <td>
              <a href="{{url('admin/komoditi/edit/' . $row->id)}}">Ubah</a> |
              <a href="{{url('admin/komoditi/delete/' . $row->id)}}" onclick="return confirm('Anda yakin ingin menghapus data ?')">Hapus</a>
            </td>
          </tr>
        @endforeach
      </table>
      <center>{{$komoditi}}</center>
    </div>
  </div>
</div>
@endsection
