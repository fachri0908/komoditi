
@extends('Admin.layout')
@section('content')
<div class="panel-heading">
  <h2>Edit komoditi {{$komoditi->nama_komoditi}}</h2>
</div>
@if(isset($_SESSION['succes_input']))
  <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>{{$_SESSION['succes_input']}}</strong>
  </div>
@endif
@if(isset($_SESSION['failed_input']))
  <div class="alert alert-danger">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>{{$_SESSION['failed_input']}} !</strong>
  </div>
@endif
@if ($errors->any())
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="panel-body">
  <div class="row">
    <div class="col-lg-12">
      <form method="post" class="form-inline" enctype="multipart/form-data">
        {{csrf_field()}}
        <div>
          <label for="nama_komoditi">Nama Komoditi : </label><br>
          <input type="text" id="nama_komoditi" value="{{$komoditi->nama_komoditi}}" name="nama_komoditi" class="form-control mb-2 mr-sm-2">
        </div><br>
        <div>
          <label for="id_kategori">Satuan : </label><br>
          <select class="form-control mb-2 mr-sm-2" name="id_kategori">
            @foreach($kategori as $row)
            <option value="{{$row -> id}}" @if($komoditi->id_kategori == $row -> id) selected @endif>{{$row->nama_kategori}}</option>
            @endforeach
          </select>
        </div>
        <div>
          <label for="id_satuan">Satuan : </label><br>
          <select class="form-control mb-2 mr-sm-2" name="id_satuan">
            @foreach($satuan as $row)
            <option value="{{$row -> id}}" @if($komoditi->id_satuan == $row->id) selected @endif>{{$row->nama_satuan}}</option>
            @endforeach
          </select>
        </div><br>
        <br>
        <div>
          <button type="submit" class="btn btn-primary">Simpan</button>
          <button type="reset" class="btn btn-warning">Reset</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
