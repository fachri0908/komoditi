<html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Komoditas Pasar</title>

    <!-- Bootstrap Core CSS -->
    <!-- Cara Panggil -->



    <link rel="stylesheet" href="{{asset('css/layout/vendor/bootstrap/css/bootstrap.min.css')}}">

    <!-- MetisMenu CSS -->
    <link rel="stylesheet" href="{{asset('css/layout/vendor/metisMenu/metisMenu.min.css')}}">

    <!-- Custom CSS -->
    <link href="{{asset('css/layout/dist/css/sb-admin-2.css')}}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{asset('css/layout/vendor/morrisjs/morris.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{asset('css/layout/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">


              <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                      <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                  </a>
                  <ul class="dropdown-menu dropdown-user">
                      <li><a href="{{url('admin/user/edit')}}"><i class="fa fa-user fa-fw"></i> Ganti Password</a>
                      </li>
                      <li class="divider"></li>
                      <li><a class="text-center" href="{{route('logout')}}">
                          <strong>Logout</strong>
                          <i class="fa fa-angle-right"></i>
                      </a>
                      </li>
                  </ul>
                  <!-- /.dropdown-user -->
              </li>

            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <li>
                            <a href="{{url('admin/pasar')}}"><i class="fa fa-building-o fa-fw"></i> Pasar</a>
                        </li>
                        <li>
                            <a href="{{url('admin/kategori')}}"><i class="fa fa-sitemap fa-fw"></i> Kategori</a>
                        </li>
                        <li>
                            <a href="{{url('admin/komoditi')}}"><i class="fa fa-tag fa-fw"></i> Komoditas</a>
                        </li>
                        <li>
                            <a href="{{url('admin/satuan')}}"><i class="fa fa-balance-scale fa-fw"></i> Satuan</a>
                        </li>
                        @if(Auth::user()->username=='dajoe')
                        <li>
                            <a href="{{url('admin/user')}}"><i class="fa fa-user fa-fw"></i> User</a>
                        </li>
                        @endif
                        <li>
                            <a href="{{url('admin/harga')}}"><i class="fa fa-money fa-fw"></i> Data Harga</a>
                        </li>
                        <li>
                            <a href="{{url('admin/harga/upload')}}"><i class="fa fa-file-excel-o fa-fw"></i> import XLS/CSV</a>
                        </li>
                        <li>
                            <a href="{{url('admin/harga/uploaded')}}"><i class="fa fa-list fa-fw"></i> Data Tersimpan</a>
                        </li>
                        <li>
                            <a href="{{url('admin/kontak')}}"><i class="fa fa-rss fa-fw"></i> Masukan</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper"><br>
            <div class="row">
                <div class="col-lg-12">
                  <div class="panel panel-default">
                    @section('content')
                    @show
                  </div>
                </div>
                <!-- /.col-lg-8 -->
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{asset('css/layout/vendor/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('css/layout/vendor/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{asset('css/layout/vendor/metisMenu/metisMenu.min.js')}}"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{asset('css/layout/vendor/raphael/raphael.min.js')}}"></script>
    <script src="{{asset('css/layout/vendor/morrisjs/morris.min.js')}}"></script>
    <script src="{{asset('css/layout/data/morris-data.js')}}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{asset('css/layout/dist/js/sb-admin-2.js')}}"></script>

</body>

</html>
