@extends('Admin.layout')
@section('content')

    <div class="panel-heading">
        <h2>Kiriman Pengunjung</h2>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                          <tr>
                            <th>No</th>
                            <th>Nama Pengirim</th>
                            <td>Email</td>
                            <td>Pesan</td>
                            <th>Aksi</th>
                          </tr>
                          @foreach($kontak as $row)
                            <tr>
                              <td>{{$loop->iteration}}</td>
                              <td>{{$row->nama}}</td>
                              <td>{{$row->email}}</td>
                              <td>{{$row->pesan}}</td>
                              <td>
                                <a href="{{url('admin/kontak/delete/' . $row->id)}}" onclick="return confirm('Anda yakin ingin menghapus data ?')">Hapus</a>
                              </td>
                            </tr>
                          @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
