@extends('Admin.layout')
@section('content')
    <div class="panel-heading">
        <h2>
          @if(app('request')->get('month'))
            Semua data Harga Bulan
          @else
            Data Pertanggal Terbaru
          @endif
          <?php echo getmonthname(app('request')->get('month')).' Tahun '.app('request')->get('year'); ?>
        </h2>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
              <?php $month=['Januari','Februari','Maret','April','Mai','Juni','Juli','Agustus','September','Oktober','November','Desember'];?>
              <form class="form-inline" method="get">
                <select class="form-control mb-2 mr-sm-2" id="month" name="month">
                  <option value="">Data terakhir</option>
                  @foreach($month as $m)
                    <option value="{{$loop->iteration}}" @if(app('request')->get('month')==$loop->iteration) selected @endif>{{$m}}</option>
                  @endforeach
                </select>
                <select class="form-control mb-2 mr-sm-2" id="tahun" name="year">
                  @foreach($year as $y)
                    <option value="{{$y->tahun}}" @if(app('request')->get('year')==$loop->iteration) selected @endif>{{$y->tahun}}</option>
                  @endforeach
                </select>
                <button class="btn btn-primary btn-sm" type="submit">Cari</button>
              </form>
              <br>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                          <tr>
                            <th>Tanggal</th>
                            <th>Jumlah Komoditi</th>
                            <th>jumlah Data (3 Pasar)</th>
                            <th>Aksi</th>
                          </tr>
                          @foreach($harga as $row)
                            <tr>
                              <td>
                                <?php getdatename(date('l', strtotime($row->tanggal))) ?>,
                                {{date("d", strtotime($row->tanggal))}}
                                <?php getmonthname(date('m', strtotime($row->tanggal)))  ?>
                                {{date("Y", strtotime($row->tanggal))}}
                              </td>
                              <td>{{$row->jumlahkomoditi}} Komoditi</td>
                              <td>{{$row->jumlahdata}} data</td>
                              <td>
                                <a href="{{url('admin/harga/item/'.$row->tanggal)}}">lihat</a>
                                 |
                                <a href="{{url('admin/harga/deleteperdate/' . $row->tanggal)}}" onclick="return confirm('Anda yakin ingin menghapus data ?')">Hapus</a>
                              </td>
                            </tr>
                          @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
