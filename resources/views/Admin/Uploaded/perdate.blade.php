@extends('Admin.layout')
@section('content')

<div class="panel-heading">
  <h2>Data Harga {{$tanggal}}</h2>
</div>
<div class="panel-body">
    <div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">
          <table class="table table-bordered">
            <tr>
              <th>Komoditi</th>
              <th>Pasar</th>
              <th>Tanggal</th>
              <th>Harga</th>
              <th>Aksi</th>
            </tr>
            @foreach($harga as $row)
              <tr>
                <td>{{$row->komoditi->nama_komoditi}}</td>
                <td>{{$row->pasar->nama_pasar}}</td>
                <td>
                   {{date('d', strtotime($row->tanggal))}}
                   <?php getmonthname(date('m', strtotime($row->tanggal)))  ?>
                   {{date("Y", strtotime($row->tanggal))}}

                </td>
                <td>{{$row->harga}}</td>
                <td>
                  <a href="{{url('admin/harga/edit/' . $row->id)}}">Ubah</a> |
                  <a href="{{url('admin/harga/delete/' . $row->id)}}" onclick="return confirm('Anda yakin ingin menghapus data ?')">Hapus</a>
                </td>
              </tr>
            @endforeach
          </table>
        </div>
      </div>
      <center>{{$harga}}</center>
    </div>
  </div>
</div>
  <div class="panel panel-default">
@endsection
