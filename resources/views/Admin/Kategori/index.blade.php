@extends('Admin.layout')
    @section('content')
    <div class="panel-heading">
      <h2>Tambah Data Kategori</h2>
    </div>
    <div class="panel-body">
      <h5><a href="{{url("/admin/kategori/new")}}" class="btn btn-primary">+Tambah Data Kategori</a></h5>
      <div class="row">
        <div class="col-lg-12">
          <table class="table table-bordered">
            <tr>
              <th>No</th>
              <th>Nama Kategori</th>
              <th>Aksi</th>
            </tr>
            @foreach($kategori as $row)
              <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$row->nama_kategori}}</td>
                <td>
                  <a href="{{url('admin/kategori/edit/' . $row->id)}}">Ubah</a> |
                  <a href="{{url('admin/kategori/delete/' . $row->id)}}" onclick="return confirm('Anda yakin ingin menghapus data ?')">Hapus</a>
                </td>
              </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
    @endsection
