@extends('WebView.base')
@section('content')
<style scoped>
.dropdown-menu.multi-column {
  width: 400px;
}

.dropdown-menu.multi-column .dropdown-menu {
  display: block !important;
  position: static !important;
  margin: 0 !important;
  border: none !important;
  box-shadow: none !important;
  .dropdown-menu {
	min-width: 200px;
}
.dropdown-menu.columns-2 {
	min-width: 400px;
}
.dropdown-menu.columns-3 {
	min-width: 600px;
}
.dropdown-menu li a {
	padding: 5px 15px;
	font-weight: 300;
}
.multi-column-dropdown {
	list-style: none;
  margin: 0px;
  padding: 0px;
}
.multi-column-dropdown li a {
	display: block;
	clear: both;
	line-height: 1.428571429;
	color: #333;
	white-space: normal;
}
.multi-column-dropdown li a:hover {
	text-decoration: none;
	color: #262626;
	background-color: #999;
}

@media (max-width: 767px) {
	.dropdown-menu.multi-column {
		min-width: 240px !important;
		overflow-x: hidden;
	}
}
}
</style>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      var harga = <?php echo $harga; ?>;
      var komoditiname = <?php echo $komoditi; ?>;
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable(harga);
        var options = {
          curveType: 'function',
          legend: { position: 'bottom' },
          colors: ['blue', 'red', 'green', 'brown'],
          pointSize: 6,
          hAxis:{
            direction: '-1'
          },
          vAxis: {
            minValue:0, maxValue:18000
          },
          animation: {
            "startup": true,
            duration: 800,
            easing: 'out',
          },
          annotation:{
            style:'default',
          }
        };
        var chart = new google.visualization.LineChart(document.getElementById('linechart'));
        chart.draw(data, options);
        setTimeout(function(){
          chart.draw(data, options);
        },1000);
      }
      function myFunction() {
        var mindate=document.getElementById("mindate").value;
        var maxdate=document.getElementById("maxdate").value;
        var id_komoditi = <?php echo app('request')->input('id_komoditi') ?>;
        location.href='komoditi?id_komoditi='+id_komoditi+'&mindate='+mindate+'&maxdate='+maxdate;
          // document.write(document.getElementById("mindate").value);
      }
    </script>

  <div class="container">
    <br>
    <div style="margin-left:170px">
      <ul class="navbar-nav mr-auto">
          <li class="nav-item dropdown btn-group">
              <a class="nav-link dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{json_decode($komoditi)}} &raquo; </a>
              <div class="dropdown-menu dropdown" aria-labelledby="dropdownMenu1">
                  <div class="container-fluid">
                      <div class="row">
                          <div class="col-md-4-fluid" style="padding-left:0px;padding-right:0px">
                            @foreach($listkomoditi as $row)
                            <a class="dropdown-item" href="{{url('grafik/komoditi/?id_komoditi='.$row->id.'&mindate='.app('request')->input('mindate').'&maxdate='.app('request')->input('maxdate'))}}">{{$row->nama_komoditi}}</a>
                            @if($loop->iteration %15 ==0)
                          </div>
                          <div class="col-md-4-fluid" style="padding-left:0px;padding-right:0px">
                            @endif
                            @endforeach
                          </div>
                      </div>
                  </div>
              </div>
          </li>
      </ul>
    </div>
    <div class="table-responsive">
      <table  border="0" align="center" style="width:70%">
        <tr>
          <td>Tanggal : </td>
          <td>
            <input type="date" name="latestdate" id="latestdate" class="form-control mb-2 mr-sm-2" value="<?php echo $latestdate; ?>">
          </td>
          <td> &nbsp;&nbsp;</td>
          <td> Sampai :</td>
          <td>
            <input type="date" name="previousdate" id="previousdate" class="form-control mb-2 mr-sm-2" value="<?php echo $previousdate;?>">
          </td>
          <td>
            <button type="button" onclick="myFunction()" name="button" class="btn btn-link">Tampilkan &raquo;</button>
          </td>
        </tr>
      </table>
    </div>
          <br>
      <div id="printarea">
        <h4 align="center"> Grafik Harga {{json_decode($komoditi)}} /{{json_decode($satuan)}}</h4>
        <div id="linechart" style="width: 1000px; height: 500px; margin-left:100px"></div>
      </div>

      <script>
        function myFunction() {
          var mindate=document.getElementById("previousdate").value;
          var maxdate=document.getElementById("latestdate").value;
          var id_komoditi = <?php echo app('request')->input('id_komoditi') ?>;
          location.href='komoditi?id_komoditi='+id_komoditi+'&mindate='+mindate+'&maxdate='+maxdate;
        }
      </script>
      <script type="text/javascript" language="javascript">
      function printDiv(divID){
        var divElements = document.getElementById(divID).innerHTML;
        var oldPage=document.body.innerHTML;
        document.body.innerHTML=
        "<html><head><title></title><body>"+divElements+"</body></html>";
        window.print();
        document.body.innerHTML = oldPage;
      }
      </script>
      <div class="row" style="margin-left:200px;margin-bottom:50px">
        <div class="col-md-6">
          <!-- <button type="button" class="btn btn-info btn-sm" name="button" onclick="printDiv('printarea')">Cetak Grafik</button> -->
        </div>
      </div>
    </div>
@endsection
