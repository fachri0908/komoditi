@extends('Admin.layout')
@section('content')

<div class="panel-heading">
  <h2>Data Harga {{$harga[0]->pasar->nama_pasar}}</h2>
</div>
<div class="panel-body">
<h5><a href="{{url("/admin/harga/new")}}" class="btn btn-primary">+Tambah Data Harga</a></h5>

    <div class="row">
      <div class="col-lg-12">

        <div class="dropdown">
          <button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown">{{$harga[0]->pasar->nama_pasar}}
          <span class="caret"></span></button>
          <ul class="dropdown-menu">
            @foreach($pasar as $row)
                <li><a class="dropdown-item" href="{{url('admin/harga/pasar/' . $row->id)}}">{{$row->nama_pasar}}</a> <br></li>
            @endforeach
          </ul>
        </div><br>

        <div class="table-responsive">
          <table class="table table-bordered">
            <tr>
              <th>Komoditi</th>
              <th>Tanggal</th>
              <th>Harga</th>
              <th>Aksi</th>
            </tr>
            @foreach($harga as $row)
              <tr>
                <td>{{$row->komoditi->nama_komoditi}}</td>
                <td>{{date("d-m-Y", strtotime($row->tanggal))}}</td>
                <td>{{$row->harga}}</td>
                <td>
                  <a href="{{url('admin/harga/edit/' . $row->id)}}">Ubah</a> |
                  <a href="{{url('admin/harga/delete/' . $row->id)}}" onclick="return confirm('Anda yakin ingin menghapus data ?')">Hapus</a>
                </td>
              </tr>
            @endforeach
          </table>
        </div>
      </div>
      <center>{{$harga}}</center>
    </div>
  </div>
</div>
  <div class="panel panel-default">
@endsection
