@extends('Admin.layout')
@section('content')
<div class="panel-heading">
  <h3>Import File XLS/CSV Harga Komoditas Pangan</h3>
</div>
<div class="panel-body">
  <div class="row">
    <div class="col-lg-12">
      @if ( Session::has('success') )
      <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
          <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('success') }}</strong>
      </div>
      @endif

      @if ( Session::has('error') )
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
          </button>
          <strong>{{ Session::get('error') }}</strong>
        </div>
      @endif

      @if (count($errors) > 0)
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
          <div>
            @foreach ($errors->all() as $error)
              <p>{{ $error }}</p>
            @endforeach
          </div>
        </div>
      @endif

      <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data" class="form-inline">
        {{ csrf_field() }}
        <label for="file">Pilih File</label><br>
        <input type="file" name="file" class="form-control mb-2 mr-sm-2"><br>

        <input type="submit" class="btn btn-primary" style="margin-top: 3%">
      </form>

    </div>
  </div>
</div>
@endsection
