@extends('Admin.layout')
@section('content')
<div class="panel-heading">
  <h2>Tambah Data Harga Pasar</h2>
</div>
@if(isset($_SESSION['duplicate_input']))
  <div class="alert alert-danger">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>{{$_SESSION['duplicate_input']}}</strong>
  </div>
@endif
@if(isset($_SESSION['succes_input']))
  <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>{{$_SESSION['succes_input']}}</strong>
  </div>
@endif
@if(isset($_SESSION['failed_input']))
  <div class="alert alert-danger">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>{{$_SESSION['failed_input']}} !</strong>
  </div>
@endif
@if ($errors->any())
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="panel-body">
  <div class="row">
    <div class="col-lg-12">
      <form method="post" class="form-inline">
        {{csrf_field()}}
        <div>
          <label for="id_komoditi">Komoditi : </label><br>
          <select class="form-control mb-2 mr-sm-2" name="id_komoditi">
            @foreach($komoditi as $row)
            <option value="{{$row -> id}}">{{$row->nama_komoditi}}</option>
            @endforeach
          </select>
        </div><br>
        <div>
          <label for="id_pasar">Pasar : </label><br>
          <select class="form-control mb-2 mr-sm-2" name="id_pasar">
            @foreach($pasar as $row)
            <option value="{{$row -> id}}">{{$row->nama_pasar}}</option>
            @endforeach
          </select>
        </div><br>
        <div>
          <label for="tanggal">Tanggal : </label><br>
          <input type="date" name="tanggal" class="form-control mb-2 mr-sm-2" value="<?php echo date("Y-m-d"); ?>"></input>
        </div><br>
        <div>
          <label for="harga">Harga : </label><br>
          <input type="number" name="harga" class="form-control mb-2 mr-sm-2"></input>
        </div><br>
        <div>
          <button type="submit" class="btn btn-primary">Simpan</button>
          <button type="reset" class="btn btn-warning">Reset</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection
