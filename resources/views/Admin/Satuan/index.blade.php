@extends('Admin.layout')
@section('content')
<div class="panel-heading">
  <h2>Data Satuan</h2>
</div>
<div class="panel-body">
  <h5><a href="{{url("/admin/satuan/new")}}" class="btn btn-primary">+Tambah Data Satuan</a></h5>
  <div class="row">
    <div class="col-lg-12">
      <table class="table table-bordered">
        <tr>
          <th>No</th>
          <th>Nama Satuan</th>
          <th>Aksi</th>
        </tr>
        @foreach($satuan as $row)
          <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$row->nama_satuan}}</td>
            <td>
              <a href="{{url('admin/satuan/edit/' . $row->id)}}">Ubah</a> |
              <a href="{{url('admin/satuan/delete/' . $row->id)}}" onclick="return confirm('Anda yakin ingin menghapus data ?')">Hapus</a>
            </td>
          </tr>
        @endforeach
      </table>
    </div>
  </div>
</div>
@endsection
