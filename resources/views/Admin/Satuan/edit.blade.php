
@extends('Admin.layout')
@section('content')
<div class="panel-heading">
  <h2>Edit Satuan {{$satuan->nama_satuan}}</h2>
</div>
@if(isset($_SESSION['succes_input']))
  <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>{{$_SESSION['succes_input']}}</strong>
  </div>
@endif
@if(isset($_SESSION['failed_input']))
  <div class="alert alert-danger">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>{{$_SESSION['failed_input']}} !</strong>
  </div>
@endif
@if ($errors->any())
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="panel-body">
  <div class="row">
    <div class="col-lg-12">
      <form method="post" class="form-inline">
        {{csrf_field()}}
        <div>
          <label for="nama_satuan">Nama Satuan : </label><br>
          <input type="text" id="nama_satuan" @if($satuan) value="{{$satuan->nama_satuan}}" @endif name="nama_satuan" class="form-control mb-2 mr-sm-2">
        </div>
        <br>
        <div>
          <button type="submit" class="btn btn-primary">Simpan</button>
          <button type="reset" class="btn btn-warning">Reset</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
