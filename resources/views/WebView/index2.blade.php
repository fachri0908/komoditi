@extends('WebView.base')
@section('content')
@include('WebView.header')

<style media="screen">
  .carousel-control-prev{
    width: 50px;
    height: 50px;
    margin: auto;
    margin-left: 10px;
    background-color: #d2d6db;
    border-radius: 50%;
    border: 4px solid #edeeef;
    outline: black;
  }
  .carousel-control-next{
    width: 50px;
    height: 50px;
    margin: auto;
    margin-right: 10px;
    background-color: #d2d6db;
    border-radius: 50%;
    border: 4px solid #edeeef;
    outline: black;
  }
</style>
<section class="welcome-section section-padding section-dark  animated fadeInRight" style="padding-top:1px;padding-bottom:1px">
<div class="container">
<h3 class="my-4"><strong>Data harga Komoditas Pangan Kota Padang</strong></h3>
</div>
</section>
<!-- p1 -->
<section class="welcome-section section-padding section-dark  animated fadeInLeft" style="padding-top:10px;padding-bottom:15px">
<div class="container">
<h3><b>{{$p1name}}<b></h3>
<p>Update terakhir <?php getdatename(date('l', strtotime($p1latestdate))) ?>,
  {{date("d", strtotime($p1latestdate))}} <?php getmonthname(date('m', strtotime($p1latestdate)))  ?>
  {{date("Y", strtotime($p1latestdate))}}, dibandingkan dengan data <?php getdatename(date('l', strtotime($p1previousdate))) ?>,
  {{date("d", strtotime($p1previousdate))}} <?php getmonthname(date('m', strtotime($p1previousdate)))  ?>
  {{date("Y", strtotime($p1previousdate))}},
<div id="c1" class="carousel slide" data-ride="carousel" style=" width:100%;min-height: 325px !important;">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="card-deck">
        <?php $i=0; $totalbox=0; ?>
        @foreach($p1harga as $row)
          @if($row->harga > $row->hargasebelumnya)
            <div class="card" style="background-color:#f2b0b0; max-width: 13.45rem;min-height:6.5rem;margin-right:3px; margin-bottom:5px;">
          @elseif($row->harga < $row->hargasebelumnya)
            <div class="card" style="background-color:#7fcfef; max-width: 13.45rem;min-height:6.5rem;margin-right:3px; margin-bottom:5px;">
          @else
            <div class="card" style="background-color:#8dd6a5; max-width: 13.45rem;min-height:6.5rem; margin-right:3px; margin-bottom:5px;">
          @endif

              <a href="{{url('grafik/komoditi/?id_komoditi='.$row->id_komoditi.'&mindate='.app('request')->input('mindate').'&maxdate='.app('request')->input('maxdate'))}}" style="text-decoration:none; color:black">
                <div class="card-body">
                  <font style="font-size:14px;"><strong>{{$row->nama_komoditi}}</strong></font>
                  <hr style='margin-top:0em; margin-bottom:0em' />
                  <div class="text-right" style="font-size:17px;margin-bottom:0.5em">
                    <strong>Rp. {{number_format($row->harga, 2, ',', '.') }}</strong>
                  </div>
                    @if($row->harga > $row->hargasebelumnya)
                      <font style="font-size:14px;color:#e04343;position:absolute;bottom:0"><strong> &#9650; Naik Rp. {{number_format($row->harga - $row->hargasebelumnya, 2, ',', '.') }}</strong></font>
                    @elseif($row->harga < $row->hargasebelumnya)
                      <font style="font-size:14px;color:#1d7dd1;position:absolute;bottom:0"><strong> &#9660; Turun Rp. {{number_format($row->hargasebelumnya - $row->harga, 2, ',', '.') }}</strong></font>
                    @else
                      <font style="font-size:14px;position:absolute;bottom:0"><i class="fa fa-equals fa-fw"></i> Harga tetap</font>
                    @endif
                </div>
              </a>
            </div>
            <?php $totalbox += 1; ?>
            @if($totalbox % 5==0)
          </div>
        <div class="card-deck">
            @endif
          <?php $i +=1; ?>
          @if($totalbox %15 == 0)
          </div>
          </div>
          <div class="carousel-item">
          <div class="card-deck">
            @endif
        @endforeach
      </div>
    </div>
  </div>

  <a class="carousel-control-prev" href="#c1" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#c1" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>
</div>
</div>
</section>
<!-- end of p1 -->
<?php $totalbox=0 ?>
<!-- p2 -->
<section class="welcome-section section-padding section-dark  animated fadeInLeft" style="padding-top:10px;padding-bottom:15px">
<div class="container">
<h3><b>{{$p2name}}<b></h3>
<p>Update terakhir <?php getdatename(date('l', strtotime($p2latestdate))) ?>,
  {{date("d", strtotime($p2latestdate))}} <?php getmonthname(date('m', strtotime($p2latestdate)))  ?>
  {{date("Y", strtotime($p2latestdate))}}, dibandingkan dengan data <?php getdatename(date('l', strtotime($p2previousdate))) ?>,
  {{date("d", strtotime($p2previousdate))}} <?php getmonthname(date('m', strtotime($p2previousdate)))  ?>
  {{date("Y", strtotime($p2previousdate))}},
<div id="c2" class="carousel slide" data-ride="carousel" style=" width:100%;min-height: 325px !important;">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="card-deck">
        <?php $i=0; $totalbox=0; ?>
        @foreach($p2harga as $row)
          @if($row->harga > $row->hargasebelumnya)
            <div class="card" style="background-color:#f2b0b0; max-width: 13.45rem;min-height:6.5rem;margin-right:3px; margin-bottom:5px;">
          @elseif($row->harga < $row->hargasebelumnya)
            <div class="card" style="background-color:#7fcfef; max-width: 13.45rem;min-height:6.5rem;margin-right:3px; margin-bottom:5px;">
          @else
            <div class="card" style="background-color:#8dd6a5; max-width: 13.45rem;min-height:6.5rem; margin-right:3px; margin-bottom:5px;">
          @endif

              <a href="{{url('grafik/komoditi/?id_komoditi='.$row->id_komoditi.'&mindate='.app('request')->input('mindate').'&maxdate='.app('request')->input('maxdate'))}}" style="text-decoration:none; color:black">
                <div class="card-body">
                  <font style="font-size:14px;"><strong>{{$row->nama_komoditi}}</strong></font>
                  <hr style='margin-top:0em; margin-bottom:0em' />
                  <div class="text-right" style="font-size:17px;margin-bottom:0.5em">
                    <strong>Rp. {{number_format($row->harga, 2, ',', '.') }}</strong>
                  </div>
                    @if($row->harga > $row->hargasebelumnya)
                      <font style="font-size:14px;color:#e04343;position:absolute;bottom:0"><strong> &#9650; Naik Rp. {{number_format($row->harga - $row->hargasebelumnya, 2, ',', '.') }}</strong></font>
                    @elseif($row->harga < $row->hargasebelumnya)
                      <font style="font-size:14px;color:#1d7dd1;position:absolute;bottom:0"><strong> &#9660; Turun Rp. {{number_format($row->hargasebelumnya - $row->harga, 2, ',', '.') }}</strong></font>
                    @else
                      <font style="font-size:14px;position:absolute;bottom:0"><i class="fa fa-equals fa-fw"></i> Harga tetap</font>
                    @endif
                </div>
              </a>
            </div>
            <?php $totalbox += 1; ?>
            @if($totalbox % 5==0)
          </div>
        <div class="card-deck">
            @endif
          <?php $i +=1; ?>
          @if($totalbox %15 == 0)
          </div>
          </div>
          <div class="carousel-item">
          <div class="card-deck">
            @endif
        @endforeach
      </div>
    </div>
  </div>

  <a class="carousel-control-prev" href="#c2" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#c2" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>
</div>
</div>
</section>
<!-- end of p2 -->
<?php $totalbox=0 ?>
<!-- p2 -->
<section class="welcome-section section-padding section-dark  animated fadeInLeft" style="padding-top:10px;padding-bottom:15px">
<div class="container">
<h3><b>{{$p3name}}<b></h3>
<p>Update terakhir <?php getdatename(date('l', strtotime($p3latestdate))) ?>,
  {{date("d", strtotime($p3latestdate))}} <?php getmonthname(date('m', strtotime($p3latestdate)))  ?>
  {{date("Y", strtotime($p3latestdate))}}, dibandingkan dengan data <?php getdatename(date('l', strtotime($p3previousdate))) ?>,
  {{date("d", strtotime($p3previousdate))}} <?php getmonthname(date('m', strtotime($p3previousdate)))  ?>
  {{date("Y", strtotime($p3previousdate))}},
<div id="c3" class="carousel slide" data-ride="carousel" style=" width:100%;min-height: 325px !important;">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="card-deck">
        <?php $i=0; $totalbox=0; ?>
        @foreach($p3harga as $row)
          @if($row->harga > $row->hargasebelumnya)
            <div class="card" style="background-color:#f2b0b0; max-width: 13.45rem;min-height:6.5rem;margin-right:3px; margin-bottom:5px;">
          @elseif($row->harga < $row->hargasebelumnya)
            <div class="card" style="background-color:#7fcfef; max-width: 13.45rem;min-height:6.5rem;margin-right:3px; margin-bottom:5px;">
          @else
            <div class="card" style="background-color:#8dd6a5; max-width: 13.45rem;min-height:6.5rem; margin-right:3px; margin-bottom:5px;">
          @endif

              <a href="{{url('grafik/komoditi/?id_komoditi='.$row->id_komoditi.'&mindate='.app('request')->input('mindate').'&maxdate='.app('request')->input('maxdate'))}}" style="text-decoration:none; color:black">
                <div class="card-body">
                  <font style="font-size:14px;"><strong>{{$row->nama_komoditi}}</strong></font>
                  <hr style='margin-top:0em; margin-bottom:0em' />
                  <div class="text-right" style="font-size:17px;margin-bottom:0.5em">
                    <strong>Rp. {{number_format($row->harga, 2, ',', '.') }}</strong>
                  </div>
                    @if($row->harga > $row->hargasebelumnya)
                      <font style="font-size:14px;color:#e04343;position:absolute;bottom:0"><strong> &#9650; Naik Rp. {{number_format($row->harga - $row->hargasebelumnya, 2, ',', '.') }}</strong></font>
                    @elseif($row->harga < $row->hargasebelumnya)
                      <font style="font-size:14px;color:#1d7dd1;position:absolute;bottom:0"><strong> &#9660; Turun Rp. {{number_format($row->hargasebelumnya - $row->harga, 2, ',', '.') }}</strong></font>
                    @else
                      <font style="font-size:14px;position:absolute;bottom:0"><i class="fa fa-equals fa-fw"></i> Harga tetap</font>
                    @endif
                </div>
              </a>
            </div>
            <?php $totalbox += 1; ?>
            @if($totalbox % 5==0)
          </div>
        <div class="card-deck">
            @endif
          <?php $i +=1; ?>
          @if($totalbox %15 == 0)
          </div>
          </div>
          <div class="carousel-item">
          <div class="card-deck">
            @endif
        @endforeach
      </div>
    </div>
  </div>

  <a class="carousel-control-prev" href="#c3" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#c3" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>
</div>
</div>
</section>
<!-- end of p2 -->
<br>
@endsection
