@extends('WebView.base')
@section('content')
<br>
<section class="Material-contact-section section-padding section-dark">
  <div class="container">
      <div class="row">
          <!-- Section Titile -->
          <div class="col-md-12 wow animated fadeInLeft" data-wow-delay=".2s">
              <h1 class="section-title">Kirimkan Saran dan pertanyaan Anda</h1>
          </div>
      </div>
      @if(isset($_SESSION['succes_input']))
        <div class="alert alert-success">
      		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      	  	<strong>{{$_SESSION['succes_input']}}</strong>
      	</div>
      @endif
      @if(isset($_SESSION['failed_input']))
        <div class="alert alert-danger">
      		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      	  	<strong>{{$_SESSION['failed_input']}} !</strong>
      	</div>
      @endif
      @if ($errors->any())
          <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif
      <div class="row">

          <div class="col-md-6 wow animated fadeInRight" data-wow-delay=".2s">
              <form class="shake" role="form" method="post">
                {{csrf_field()}}
                  <!-- Name -->
                  <div class="form-group label-floating">
                    <label class="control-label" for="name">Nama</label>
                    <input class="form-control" id="nama" type="text" name="nama">
                  </div>
                  <!-- email -->
                  <div class="form-group label-floating">
                    <label class="control-label" for="email">Email</label>
                    <input class="form-control" id="email" type="email" name="email">
                  </div>
                  <!-- Message -->
                  <div class="form-group label-floating">
                      <label for="message" class="control-label">Pesan</label>
                      <textarea class="form-control" rows="3" id="pesan" name="pesan"></textarea>
                  </div>
                  <!-- Form Submit -->
                  <div class="form-submit mt-5">
                      <button class="btn btn-common" type="submit" id="form-submit"><i class="material-icons mdi mdi-message-outline"></i> Kirim Pesan</button>
                      <div id="msgSubmit" class="h3 text-center hidden"></div>
                      <div class="clearfix"></div>
                  </div>
              </form>
          </div>
      </div>
  </div>
</section>
@endsection
