@extends('WebView.base')
@section('content')
@include('WebView.header')

<style scoped>
.dropdown-menu.multi-column {
  width: 400px;
}

.dropdown-menu.multi-column .dropdown-menu {
  display: block !important;
  position: static !important;
  margin: 0 !important;
  border: none !important;
  box-shadow: none !important;
  .dropdown-menu {
	min-width: 200px;
}
.dropdown-menu.columns-2 {
	min-width: 400px;
}
.dropdown-menu.columns-3 {
	min-width: 600px;
}
.dropdown-menu li a {
	padding: 5px 15px;
	font-weight: 300;
}
.multi-column-dropdown {
	list-style: none;
  margin: 0px;
  padding: 0px;
}
.multi-column-dropdown li a {
	display: block;
	clear: both;
	line-height: 1.428571429;
	color: #333;
	white-space: normal;
}
.multi-column-dropdown li a:hover {
	text-decoration: none;
	color: #262626;
	background-color: #999;
}

@media (max-width: 767px) {
	.dropdown-menu.multi-column {
		min-width: 240px !important;
		overflow-x: hidden;
	}
}
}
</style>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      var harga = <?php echo $harga; ?>;
      var komoditiname = <?php echo $komoditi; ?>;
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable(harga);
        var options = {
          curveType: 'function',
          legend: { position: 'bottom' },
          colors: ['blue', 'red', 'green', 'brown'],
          pointSize: 6,
          hAxis:{
            direction: '-1'
          },
          vAxis: {
            minValue:0, maxValue:18000
          },
          animation: {
            "startup": true,
            duration: 800,
            easing: 'out',
          },
          annotation:{
            style:'default',
          },
        };
        var chart = new google.visualization.LineChart(document.getElementById('linechart'));
        chart.draw(data, options);
        setTimeout(function(){
          chart.draw(data, options);
        },1000);
      }
      function myFunction() {
        var m1=document.getElementById("m1").value;
        var m2=document.getElementById("m2").value;
        var id_komoditi = <?php echo app('request')->input('id_komoditi') ?>;
        location.href='komoditi?id_komoditi='+id_komoditi+'&m1='+m1+'&m2='+m2;
          // document.write(document.getElementById("m1").value);
      }
    </script>
    <section class="Material-about-section section-padding wow animated fadeInRight" style="padding:0px">
      <div class="container">
    <br>
    <div style="margin-left:170px">
      <ul class="navbar-nav mr-auto">
          <li class="nav-item dropdown btn-group">
              <a class="nav-link dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{json_decode($komoditi)}}  &raquo; </a>
              <div class="dropdown-menu dropdown" aria-labelledby="dropdownMenu1">
                  <div class="container-fluid">
                      <div class="row">
                          <div class="col-md-4-fluid">
                            @foreach($listkomoditi as $row)
                            <a class="dropdown-item" href="{{url('grafikbulanan?id_komoditi='.$row->id.'&m1='.app('request')->input('m1').'&m2='.app('request')->input('m2').'&y='.app('request')->input('y'))}}">{{$row->nama_komoditi}}</a>
                            @if($loop->iteration %15 ==0)
                          </div>
                          <div class="col-md-4-fluid">
                            @endif
                            @endforeach
                          </div>
                      </div>
                  </div>
              </div>
          </li>
      </ul>
    </div>


    <div class="table-responsive">
      <table  border="0" align="center" style="width:70%">
        <tr>
          <td>
            <select name="month1" id="month1" class="form-control mb-2 mr-sm-2">
              @for($i=1; $i < 13; $i++)
                <option value="{{$i}}" @if($i==app('request')->input('m2')) selected @endif><?php echo getmonthname($i) ?> </option>
              @endfor
            </select>
          </td>
          <td> &nbsp;&nbsp;</td>
          <td> Sampai :</td>
          <td>
            <select name="month2" id="month2" class="form-control mb-2 mr-sm-2">
              @for($i=1; $i < 13; $i++)
                <option value="{{$i}}" @if($i==app('request')->input('m1')) selected @endif><?php echo getmonthname($i) ?> </option>
              @endfor
            </select>
          </td>
          <td> &nbsp;&nbsp;</td>
          <td> Tahun : </td>
          <td>
            <select name="year" id="year" class="form-control mb-2 mr-sm-2">
              @foreach($listtahun as $l)
                <option value="{{$l->tahun}}"
                  @if($l==app('request')->input('y')) selected @endif>
                  {{$l->tahun}}</option>
              @endforeach
            </select>
          </td>
          <td>
            <button type="button" onclick="myFunction()" name="button" class="btn btn-link">Tampilkan &raquo;</button>
          </td>
        </tr>
      </table>
    </div>
    <br>
    <div id="printarea">
      <h4 align="center"> Grafik Harga Rata - Rata {{json_decode($komoditi)}} /{{json_decode($satuan)}} <br><br>
        {{getmonthname(app('request')->input('m2'))}}-{{getmonthname(app('request')->input('m1'))}}({{app('request')->input('y')}})</h4>
      <div id="linechart" style="width: 1000px; height: 500px; margin-left:100px"></div>
    </div>

      <script>
        function myFunction() {
          var m1=document.getElementById("month2").value;
          var m2=document.getElementById("month1").value;
          var y=document.getElementById("year").value;
          var id_komoditi = <?php echo app('request')->input('id_komoditi') ?>;
          location.href='grafikbulanan?id_komoditi='+id_komoditi+'&m1='+m1+'&m2='+m2+'&y='+y;
        }
      </script>
      <script type="text/javascript" language="javascript">
      function printDiv(divID){
        var divElements = document.getElementById(divID).innerHTML;
        var oldPage=document.body.innerHTML;
        document.body.innerHTML=
        "<html><head><title></title><body>"+divElements+"</body></html>";
        window.print();
        document.body.innerHTML = oldPage;
      }
      </script>
      <div class="row" style="margin-left:200px;margin-bottom:50px">
        <div class="col-md-6">
          <img src="{{asset('css/base/assets/images/si-glyph-print.svg')}}" style="width:8%;height:8%"><button type="button" class="btn btn-info btn-lg" name="button" onclick="printDiv('printarea')">Cetak Grafik</button></img>
        </div>
      </div>
    </div>
  </section>
@endsection
