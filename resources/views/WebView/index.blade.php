@extends('WebView.base')
@section('content')
@include('WebView.header')

<style media="screen">
  .carousel-control-prev{
    width: 50px;
    height: 50px;
    margin: auto;
    margin-left: 10px;
    background-color: #d2d6db;
    border-radius: 50%;
    border: 4px solid #edeeef;
    outline: black;
  }
  .carousel-control-next{
    width: 50px;
    height: 50px;
    margin: auto;
    margin-right: 10px;
    background-color: #d2d6db;
    border-radius: 50%;
    border: 4px solid #edeeef;
    outline: black;
  }
</style>
<section class="welcome-section section-padding section-dark  animated fadeInRight" style="padding-top:1px;padding-bottom:1px">
<div class="container">
<h3 class="my-4">Data harga Komoditas Pangan Kota Padang</h3>

<p>Update terakhir <?php getdatename(date('l', strtotime($lastdate[0]->tanggal))) ?>,
  {{date("d", strtotime($lastdate[0]->tanggal))}} <?php getmonthname(date('m', strtotime($lastdate[0]->tanggal)))  ?> {{date("Y", strtotime($lastdate[0]->tanggal))}},
  dibandingkan dengan data <?php getdatename(date('l', strtotime($lastdate[1]->tanggal))) ?>, {{date("d", strtotime($lastdate[1]->tanggal))}} <?php getmonthname(date('m', strtotime($lastdate[1]->tanggal)))  ?> {{date("Y", strtotime($lastdate[1]->tanggal))}},
</div>
</section>
@foreach($pasar as $p)
<section class="welcome-section section-padding section-dark  animated fadeInLeft" style="padding-top:10px;padding-bottom:15px">
<div class="container">
<h3>{{$p->nama_pasar}}</h3>
<div id="{{$p->id}}" class="carousel slide" data-ride="carousel" style=" width:100%;min-height: 325px !important;">
  <div class="carousel-inner">
    <div class="carousel-item active">

      <div class="card-deck">
        <?php $i=0; $totalbox=0; ?>
        @foreach($harga as $row)
          @if($p->id == $row->id_pasar)
          @if($row->harga > $harga2[$i]->harga)
            <div class="card" style="background-color:#f2b0b0; max-width: 13.45rem;min-height:6.5rem;margin-right:3px; margin-bottom:5px;">
          @elseif($row->harga < $harga2[$i]->harga)
            <div class="card" style="background-color:#7fcfef; max-width: 13.45rem;min-height:6.5rem;margin-right:3px; margin-bottom:5px;">
          @else
            <div class="card" style="background-color:#8dd6a5; max-width: 13.45rem;min-height:6.5rem; margin-right:3px; margin-bottom:5px;">
          @endif

              <a href="{{url('grafik/komoditi/?id_komoditi='.$row->id_komoditi.'&mindate='.app('request')->input('mindate').'&maxdate='.app('request')->input('maxdate'))}}" style="text-decoration:none; color:black">
              <div class="card-body">
                <font style="font-size:14px;"><strong>{{$row->nama_komoditi}} /{{$row->nama_satuan}}</strong></font>
                <hr style='margin-top:0em; margin-bottom:0em' />
                <div class="text-right" style="font-size:17px;margin-bottom:0.5em">
                  <strong>Rp. {{number_format($row->harga, 2, ',', '.') }}</strong>
                </div>
                  @if($row->harga > $harga2[$i]->harga)
                    <font style="font-size:14px;color:#e04343;position:absolute;bottom:0"><strong> &#9650; Naik Rp. {{number_format($row->harga - $harga2[$i]->harga, 2, ',', '.') }}</strong></font>
                  @elseif($row->harga < $harga2[$i]->harga)
                    <font style="font-size:14px;color:#1d7dd1;position:absolute;bottom:0"><strong> &#9660; Turun Rp. {{number_format($harga2[$i]->harga - $row->harga, 2, ',', '.') }}</strong></font>
                  @else
                    <font style="font-size:14px;position:absolute;bottom:0"><i class="fa fa-equals fa-fw"></i> Harga tetap</font>
                  @endif
              </div>
            </a>
            </div>
            <?php $totalbox += 1; ?>
            @if($totalbox % 5==0)
          </div>
        <div class="card-deck">
            @endif
          @endif
          <?php $i +=1; ?>
          @if($totalbox %15 == 0 && $p->id == $row->id_pasar)
          </div>
          </div>
          <div class="carousel-item">
          <div class="card-deck">
          @endif
        @endforeach
      </div>
    </div>
  </div>

  <a class="carousel-control-prev" href="#{{$p->id}}" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#{{$p->id}}" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>
</div>
</div>
</section>


@endforeach
<br>
@endsection
