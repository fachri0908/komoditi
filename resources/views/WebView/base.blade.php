<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Sistem Informasi Harga Pangan Kota Padang</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/base/assets/css/bootstrap.min.css')}}">
    <!-- Meterial Icon CSS -->
    <link rel="stylesheet" href="{{asset('css/base/assets/css/materialdesignicons.min.css')}}">
    <!-- Material CSS -->
    <link rel="stylesheet" href="{{asset('css/base/assets/css/material.min.css')}}">
    <!-- Ripples CSS -->
    <link rel="stylesheet" href="{{asset('css/base/assets/css/ripples.min.css')}}">
    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="{{asset('css/base/assets/css/owl.carousel.css')}}">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{asset('css/base/assets/css/magnific-popup.css')}}">
    <!-- Slicknav CSS -->
    <link rel="stylesheet" href="{{asset('css/base/assets/css/slicknav.css')}}">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{asset('css/base/assets/css/animate.css')}}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{asset('css/base/assets/css/style.css')}}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{asset('css/base/assets/css/responsive.css')}}">


    <!-- Color CSS Styles  -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/base/assets/css/colors/indigo.css')}}" media="screen" />

  </head>
  <body>

    <!-- Header Start -->
    <header id="header">
      <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar nav-bg">
        <div class="container">
         <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <a class="navbar-brand" href="{{url('/')}}">SIKOPAN</a>
          </div>
          <div class="collapse navbar-collapse" id="main-navbar">
            <ul class="navbar-nav mr-auto w-100 justify-content-end">
              <li class="nav-item active">
                <a class="nav-link" href="{{url('grafik/komoditi?id_komoditi=1&mindate=&maxdate=')}}">
                  Grafik Harian
                </a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="{{url('grafikbulanan?id_komoditi=1&m1=12&m2=1&y=2018')}}">
                  Grafik Bulanan
                </a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="{{url('kontak')}}">
                  Kontak
                </a>
              </li>
            </ul>
          </div>
        </div>
        <!-- Mobile Menu Start -->
        <ul class="wpb-mobile-menu">
          <li>
            <a class="active" href="{{url('/')}}">
            SIKOPAN
            </a>
          </li>
          <li>
            <a class="active" href="{{url('grafik/komoditi?id_komoditi=1&mindate=&maxdate=')}}">
            Grafik Harian
            </a>
          </li>
          <li>
            <a class="active" href="{{url('grafikbulanan?id_komoditi=1&m1=12&m2=1&y=2018')}}">
            Grafik Bulanan
            </a>
          </li>
          <li>
            <a class="active" href="{{url('kontak')}}">
            Kontak
            </a>
          </li>
        </ul>
        <!-- Mobile Menu End -->
      </nav>
    </header>
    <!-- Header End -->

    <!-- Call to action Section -->

    <!-- Call to action Section End -->
    <!-- About Section -->
    <!-- About Section End -->
    @section('content')
    @show
    <!-- Services Section -->
    <!-- Services Section End -->
    <!-- work-counter area -->
    <!--Footer-->
    <footer class="page-footer center-on-small-only  pt-0 footer-widget-container">
      <!--Footer Links-->
      <div class="container pt-5 mb-5">
        <div class="row">
                    <!--Third column-->
          <div class="col-md-12 col-lg-16 col-xl-6 link-widget">
            <h3 class="footer-title">Tentang</h3>
            <ul>
              <p>Sistem Informasi Komoditas Pangan Kota Padang</p>

            </ul>
          </div>
          <!--/.Third column-->
        </div>
      </div>
      <!--/.Footer Links-->

      <!-- Copyright-->
      <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>&copy; 2018 | by Dajoe</p>
                </div>
            </div>
          </div>
      </div>
      <!--/.Copyright -->

    </footer>
    <!--/.Footer-->


    <!-- Back To Top -->
    <a href="#" class="back-to-top">
      <div class="ripple-container"></div>
      <i class="mdi mdi-arrow-up">
      </i>
    </a>

    <!-- Preloader -->
    <div id="preloader">
      <div class="loader" id="loader-1"></div>
    </div>
    <!-- End Preloader -->

    <!-- Optional JavaScript -->
    <script src="{{asset('css/base/assets/js/jquery-min.js')}}"></script>
    <script src="{{asset('css/base/assets/js/popper.min.js')}}"></script>
    <script src="{{asset('css/base/assets/js/bootstrap.min.js')}}"></script>

    <script src="{{asset('css/base/assets/js/jquery.mixitup.min.js')}}"></script>
    <script src="{{asset('css/base/assets/js/jquery.inview.js')}}"></script>
    <script src="{{asset('css/base/assets/js/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('css/base/assets/js/scroll-top.js')}}"></script>
    <script src="{{asset('css/base/assets/js/smoothscroll.js')}}"></script>
    <script src="{{asset('css/base/assets/js/material.min.js')}}"></script>
    <script src="{{asset('css/base/assets/js/ripples.min.js')}}"></script>
    <script src="{{asset('css/base/assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('css/base/assets/js/form-validator.min.js')}}"></script>
    <script src="{{asset('css/base/assets/js/contact-form-script.min.js')}}"></script>
    <script src="{{asset('css/base/assets/js/wow.js')}}"></script>
    <script src="{{asset('css/base/assets/js/jquery.vide.js')}}"></script>
    <script src="{{asset('css/base/assets/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('css/base/assets/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('css/base/assets/js/main.js')}}"></script>

  </body>
</html>
