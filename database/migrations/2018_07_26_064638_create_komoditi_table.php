<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKomoditiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('komoditi', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('id_kategori')->unsigned();
          $table->integer('id_satuan')->unsigned();
          $table->string('nama_komoditi', 50);
          $table->string('gambar', 100);
          $table->foreign('id_kategori')->references('id')->on('kategori');
          $table->foreign('id_satuan')->references('id')->on('satuan');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kategori');
    }
}
