<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHargaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('harga', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_komoditi')->unsigned();
            $table->integer('id_pasar')->unsigned();
            $table->double('harga');
            $table->double('harga_kemarin');
            $table->date('tanggal');
            $table->timestamps();
            $table->foreign('id_komoditi')->references('id')->on('komoditi');
            $table->foreign('id_pasar')->references('id')->on('pasar');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('harga');
    }
}
