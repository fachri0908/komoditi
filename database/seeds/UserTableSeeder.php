<?php

use Illuminate\Database\Seeder;
use App\UserModel;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserModel::create([
          'username' => 'dajoe',
          'password' => Hash::make('dajoe'),
          'remember_token' => str_random(20)
        ]);
    }
}
