<?php


Route::group(['middleware' => 'auth'], function() {
  Route::get('/admin', function () {
    return view('Admin.layout');
  });
  Route::get('/admin/pasar', 'PasarController@index')->name('pasar');
  Route::match(['get','post'],'/admin/pasar/new', 'PasarController@addPasar');
  Route::match(['get','post'],'/admin/pasar/edit/{id}', 'PasarController@editPasar');
  Route::get('/admin/pasar/delete/{id}', 'PasarController@deletePasar');

  Route::get('/admin/satuan', 'SatuanController@index');
  Route::match(['get','post'],'/admin/satuan/new', 'SatuanController@addSatuan');
  Route::match(['get','post'],'/admin/satuan/edit/{id}', 'SatuanController@editSatuan');
  Route::get('/admin/satuan/delete/{id}', 'SatuanController@deleteSatuan');

  Route::get('/admin/kategori', 'KategoriController@index');
  Route::match(['get','post'],'/admin/kategori/new', 'KategoriController@addKategori');
  Route::match(['get','post'],'/admin/kategori/edit/{id}', 'KategoriController@editKategori');
  Route::get('/admin/kategori/delete/{id}', 'KategoriController@deleteKategori');

  Route::get('/admin/user', 'UserController@index');
  Route::match(['get','post'],'/admin/user/new', 'UserController@addUser');
  Route::match(['get','post'],'/admin/user/edit', 'UserController@editUser');
  Route::get('/admin/user/delete/{id}', 'UserController@deleteUser');

  Route::get('/admin/komoditi', 'KomoditiController@index');
  Route::match(['get','post'],'/admin/komoditi/new', 'KomoditiController@addKomoditi');
  Route::match(['get','post'],'/admin/komoditi/edit/{id}', 'KomoditiController@editKomoditi');
  Route::get('/admin/komoditi/delete/{id}', 'KomoditiController@deleteKomoditi');

  Route::get('/admin/harga', 'HargaController@index');
  Route::get('/admin/harga/pasar/{id}', 'HargaController@perpasar')->name('perpasar');
  Route::match(['get','post'],'/admin/harga/new', 'HargaController@addHarga');
  Route::match(['get','post'],'/admin/harga/edit/{id}', 'HargaController@editHarga');
  Route::get('/admin/harga/delete/{id}', 'HargaController@deleteHarga');

  Route::get('/admin/harga/upload', 'FileUploadController@index')->name('index');
  Route::post('admin/harga/import', 'FileUploadController@import')->name('import');
  Route::get('admin/harga/deleteperdate/{tanggal}', 'HargaController@deleteperdate');

  Route::get('admin/harga/uploaded', 'HargaController@uploaded');
  Route::get('admin/harga/item/{tanggal}', 'HargaController@perdate');
  Route::get('admin/kontak', 'KontakController@index');
  Route::get('admin/kontak/delete/{id}', 'KontakController@delete');

});
Route::match(['get','post'],'/login', 'LoginController@authenticate')->name('login');

Route::get('/logout', 'LoginController@logout')->name('logout');

// Route::get('/', 'ViewController@index');
Route::get('/', 'ViewController@indextry');
Route::match(['get','post'],'/komoditi/{id}', 'ViewController@search');

Route::get('/grafik/komoditi', 'ChartController@getchart')->name('getchart');
Route::get('/grafikbulanan', 'GrafikBulananController@getchart');
Route::get('/test', 'ViewController@test');

Route::get('/cari', 'SearchController@autoComplete');

Route::match(['get','post'],'/kontak', 'KontakController@add');
