<?php

namespace App\Http\Controllers;
use App\SatuanModel;
use Illuminate\Http\Request;

class SatuanController extends Controller
{
  public function index() {
    $data = array('satuan' => SatuanModel::all());
    return view('Admin.Satuan.index', $data);
  }
  public function addSatuan(Request $request){
    if($request->isMethod('post')){
      $this->validate($request, [
        'nama_satuan' => 'required'
      ]);
      $data = array(
        'nama_satuan' => $request->input('nama_satuan')
      );
      try {
        $add = SatuanModel::insert($data);
        $_SESSION['succes_input']= "Data satuan berhasil ditambahkan";
      } catch (\Exception $e) {
        $_SESSION['failed_input']= "Gagal menambahkan data satuan, periksa kembali input data anda";
      }
    }
    return view('Admin.Satuan.add');
  }
  public function editSatuan($id=0, Request $request){
    $satuan = SatuanModel::find($id);
    if($request->isMethod('post')){
      $this->validate($request, [
        'nama_satuan' => 'required'
      ]);
      $satuan->nama_satuan=$request->input('nama_satuan');
      try {
        $edit = $satuan->save();
        $_SESSION['succes_input']= "Data satuan berhasil diperbarui";
      } catch (\Exception $e) {
        $_SESSION['failed_input']= "Gagal memperbarui data satuan, periksa kembali input data anda";
      }
    }
    return view('Admin.Satuan.edit', array('satuan' => $satuan));
  }
  public function deleteSatuan($id){
    $satuan = SatuanModel::find($id);
    try {
      $satuan->delete();
      echo "<script>alert('Data berhasil dihapus')</script>";
      return redirect()->back();
    } catch (\Exception $e) {
      echo "<script>alert('Gagal Menghapus Data')</script>";
      return redirect()->back();
    }
  }
}
