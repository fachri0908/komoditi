<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\PasarModel;
use App\HargaModel;
use Session;
use Excel;
use File;

class FileUploadController extends Controller
{
    public function index()
    {
        return view('Admin.Harga.upload');
    }

    public function import(Request $request){
        //validate the xls file
        $this->validate($request, array(
            'file'      => 'required'
        ));

        if($request->hasFile('file')){
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                if(!empty($data) && $data->count()){

                  $arrnama_pasar=['pasar_raya','pasar_nanggalo','pasar_lubuk_buaya'];
                  $idpasar=PasarModel::all();
                  $arridpasarex=[$idpasar[0]->id,$idpasar[1]->id,$idpasar[2]->id];
                  for ($i=0; $i <3; $i++) {
                    $pasar = $arrnama_pasar[$i];
                    foreach ($data as $key => $value) {
                      $tanggal = date("Y-m-d", strtotime($value->tanggal));
                      $checkduplicate=HargaModel::where('id_komoditi', $value->id_komoditi)->where('id_pasar', $arridpasarex[$i])->where('tanggal', $value->tanggal)->get();
                      $ckd=DB::select(DB::raw("SELECT id FROM `harga` where id_komoditi='$value->id_komoditi' and id_pasar='$arridpasarex[$i]' and tanggal ='
                        $value->tanggal'"));
                      if (count($ckd)>0) {
                        Session::flash('error', 'Data Duplikat..');
                        $ckd='';
                        return back();

                      }
                        $insert[] = [
                        'id_komoditi' => $value->id_komoditi,
                        'id_pasar' => $arridpasarex[$i],
                        'tanggal' => $value->tanggal,
                        'harga' => $value->$pasar,
                        'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                        'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
                        ];
                    }
                  }

                    if(!empty($insert)){
                        $insertData = DB::table('harga')->insert($insert);
                        $insert='';
                        if ($insertData) {
                            Session::flash('success', 'Your Data has successfully imported');
                        }else {
                            Session::flash('error', 'Error inserting the data..');
                            return back();
                        }
                    }
                }

                return back();

            }else {
                Session::flash('error', 'File is a '.$extension.' file.!! Please upload a valid xls/csv file..!!');
                return back();
            }
        }
    }


}
