<?php

namespace App\Http\Controllers;
use App\KategoriModel;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
  public function index() {
    $data = array(
      'kategori' => KategoriModel::all(),
    );
    return view('Admin.Kategori.index', $data);
  }
  public function addKategori(Request $request){
    if($request->isMethod('post')){
      $request->validate([
        'nama_kategori' => 'required'
      ]);
      $data = array(
        'nama_kategori' => $request->input('nama_kategori')
      );
      try {
        $add = KategoriModel::insert($data);
        $_SESSION['succes_input']= "Data kategori berhasil ditambahkan";
      } catch (\Exception $e) {
        $_SESSION['failed_input']= "Gagal menambahkan data kategori, periksa kembali input data anda";
      }
    }
    return view('Admin.Kategori.add');
  }
  public function editKategori($id, Request $request){
    $kategori = KategoriModel::find($id);
    if($request->isMethod('post')){
      $request->validate([
        'nama_kategori' => 'required'
      ]);
      $kategori->nama_kategori=$request->input('nama_kategori');
      try {
        $edit = $kategori->save();
        $_SESSION['succes_input']= "Data kategori berhasil diperbarui";
      } catch (\Exception $e) {
        $_SESSION['failed_input']= "Gagal memperbarui data kategori, periksa kembali input data anda";
      }
    }
    return view('Admin.Kategori.edit', array('kategori' => $kategori));
  }
  public function deleteKategori($id){
    $kategori = kategoriModel::find($id);
    $delete = $kategori->delete();
    if($delete){
      echo "<script>alert('Data Berhasil Dihapus')</script>";
      echo '<meta http-equiv="refresh" content="0;url='.url('admin/kategori').'">';
    }
  }
}
