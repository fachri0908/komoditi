<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KomoditiModel;

class SearchController extends Controller
{
  public function autoComplete(Request $request) {
    return KomoditiModel::where('nama_komoditi', 'LIKE', '%'.$request->q.'%')->get();
  }
}
