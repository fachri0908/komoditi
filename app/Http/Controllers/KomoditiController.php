<?php

namespace App\Http\Controllers;
use App\KomoditiModel;
use App\KategoriModel;
use App\SatuanModel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use File;

class KomoditiController extends Controller
{
  public function index() {
    $data = array(
      'komoditi' => KomoditiModel::with('satuan')->paginate(20),
    );
    $komoditi=KomoditiModel::all();
    return view('Admin.Komoditi.index', $data);
  }
  public function addKomoditi(Request $request){
    $kategori = array('kategori' => KategoriModel::all());
    $satuan = array('satuan' => SatuanModel::all());
    if($request->isMethod('post')){
      $this->validate($request, [
        'nama_komoditi' => 'required',
        'gambar' => 'image|nullable|max:1999'
      ]);
      if ($request->hasFile('gambar')) {
        //getfile name with extension
        $image=$request->file('gambar');
        $filenameWithExt = $request->file('gambar')->getClientOriginalName();
        // get just filename
        $filename=pathinfo($filenameWithExt, PATHINFO_FILENAME);
        // get just ext
        $extension = $request->file('gambar')->getClientOriginalExtension();
        //filenameto dtore
        $destination_path=public_path('/images');
        $fileNameToStore = $filename.'-'.time().'.'.$extension;

        //upload image
        $image->move($destination_path, $fileNameToStore);
      }else {
        $fileNameToStore = 'noimage.jpg';
      }
      $data = array(
        'nama_komoditi' => $request->input('nama_komoditi'),
        'id_kategori' => $request->input('id_kategori'),
        'id_satuan' => $request->input('id_satuan'),
        'gambar' => $fileNameToStore
      );
      try {
        $add = KomoditiModel::insert($data);
        $_SESSION['succes_input']= "Data komoditi berhasil ditambahkan";
      } catch (\Exception $e) {
        $_SESSION['failed_input']= "Gagal menambahkan data komoditi, periksa kembali input data anda";
      }
    }
    return view('Admin.Komoditi.add', $kategori, $satuan);
  }
  public function editKomoditi($id, Request $request){
    $komoditi = KomoditiModel::find($id);
    $kategori = KategoriModel::all();
    $satuan = SatuanModel::all();
    if($request->isMethod('post')){
      $this->validate($request, [
        'nama_komoditi' => 'required',
        'gambar' => 'image|nullable|max:1999'
      ]);
      if ($request->hasFile('gambar')) {
        //getfile name with extension
        $filenameWithExt = $request->file('gambar')->getClientOriginalName();
        // get just filename
        $filename=pathinfo($filenameWithExt, PATHINFO_FILENAME);
        // get just ext
        $extension = $request->file('gambar')->getClientOriginalExtension();
        //filenameto dtore
        $fileNameToStore = $filename.'-'.time().'.'.$extension;
        //upload image
        $path = $request->file('gambar')->storeAs('public/gambar', $fileNameToStore);
        if ($komoditi->gambar != 'noimage.jpg') {
          //delete image
          Storage::delete('public/gambar/'.$komoditi->gambar);
        }
      }

      $komoditi->nama_komoditi=$request->input('nama_komoditi');
      $komoditi->id_kategori=$request->input('id_kategori');
      $komoditi->id_satuan=$request->input('id_satuan');
      if ($request->hasFile('gambar')) {
        $komoditi->gambar = $fileNameToStore;
      }
      try {
        $edit = $komoditi->save();
        $_SESSION['succes_input']= "Data komoditi berhasil diperbarui";
      } catch (\Exception $e) {
        $_SESSION['failed_input']= "Gagal memperbarui data komoditi, periksa kembali input data anda";
      }
    }
    return view('Admin.Komoditi.edit', array('komoditi' => $komoditi, 'kategori' => $kategori, 'satuan' => $satuan));
  }
  public function deletekomoditi($id){
    $komoditi = KomoditiModel::find($id);
    if ($komoditi->gambar != 'noimage.jpg') {
      $destination_path=public_path('/images');
      File::delete($destination_path.'/'.$komoditi->gambar);
    }
    $delete = $komoditi->delete();
    if($delete){
      echo "<script>alert('Data Berhasil Dihapus')</script>";
      echo '<meta http-equiv="refresh" content="0;url='.url('admin/komoditi').'">';
    }
  }
}
