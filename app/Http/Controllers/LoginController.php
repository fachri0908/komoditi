<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
  public function authenticate(Request $request)
  {
    if ($request->isMethod('post')) {
      $this->validate($request, [
        'username' => 'required',
        'password' => 'required'
      ]);
      if(Auth::attempt(['username' => $request->username, 'password' => $request->password])){
        return redirect('/admin');
      } else {
        echo "<script>alert('Username atau Password salah!!')</script>";
      }
    }
    return view('Admin.login');
  }
  public function index(){
    return view('Admin.login');
  }
  public function logout(){
    Auth::logout();
    return redirect('/login');
  }
}
