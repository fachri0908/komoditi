<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HargaModel;
use App\KomoditiModel;
use App\PasarModel;
use DB;

class ChartController extends Controller
{

    public function getchart(){
      if(app('request')->input('id_komoditi') == ''){
        $komoditi = KomoditiModel::limit(1)->get();
        $idkomoditi = $komoditi[0]->id;
      }else {
        $idkomoditi = app('request')->input('id_komoditi');
        $komoditi = KomoditiModel::where('id', app('request')->input('id_komoditi'))->get();
      }
      $lastdate = HargaModel::groupBy('tanggal')->orderBy('tanggal' ,'desc')->limit(2)->get();
      if(app('request')->input('mindate') == ''){
        $mindate = $lastdate[0]->tanggal;
      }else {
        $mindate = app('request')->input('mindate');
      }
      if(app('request')->input('maxdate') == ''){
        $maxdate = $lastdate[1]->tanggal;
      }else {
        $maxdate = app('request')->input('maxdate');
      }
      $harga3 = DB::select(DB::raw("select komoditi.nama_komoditi,hargaa.id_komoditi,
      hargaa.tanggal, hargaa.harga as pasarraya, hargab.harga as pasarnanggalo,
      hargac.harga as pasarlubukbuaya from harga as hargaa, harga as hargab , harga as hargac, komoditi
      where hargaa.id_komoditi = hargab.id_komoditi and hargaa.id_komoditi = hargac.id_komoditi
      and hargaa.tanggal = hargab.tanggal and hargaa.tanggal = hargac.tanggal and hargaa.id_pasar =1
      and hargab.id_pasar=2 and hargac.id_pasar=3 AND komoditi.id=hargaa.id_komoditi and (hargaa.tanggal between '$maxdate' and '$mindate')
      and hargaa.id_komoditi='$idkomoditi' ORDER BY `hargaa`.`tanggal` DESC"));

        $result2[]= ['Tanggal','Pasar Raya', 'Pasar Nanggalo', 'Pasar Lubuk Buaya'];
        foreach ($harga3 as $key => $value) {
            $result2[++$key] = [date("d-M-Y", strtotime($value->tanggal)), (int)$value->pasarraya, (int)$value->pasarnanggalo, (int)$value->pasarlubukbuaya];
        }
        return view('WebView.grafik')
                ->with('harga',json_encode($result2))
                ->with('komoditi',json_encode($komoditi[0]->nama_komoditi))
                ->with('satuan',json_encode($komoditi[0]->satuan->nama_satuan))
                ->with('listkomoditi', KomoditiModel::orderBy('nama_komoditi')->get())
                ->with('latestdate', $maxdate)
                ->with('previousdate', $mindate);
    }

}
