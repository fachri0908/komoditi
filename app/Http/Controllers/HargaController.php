<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KomoditiModel;
use App\PasarModel;
use App\HargaModel;
use DB;
class HargaController extends Controller
{
  public function index() {
    $pasar=PasarModel::all();
    $data = array(
      'harga' => HargaModel::where('id_pasar', $pasar[0]->id)->with('komoditi')->with('pasar')->orderBy('tanggal', 'desc')->paginate(10),
    );
    return view('Admin.Harga.index', $data, array('pasar' => $pasar));
  }
  public function perpasar($id, Request $request){
    $pasar=PasarModel::all();
    $data = array(
      'harga' => HargaModel::where('id_pasar', $id)->with('komoditi')->with('pasar')->orderBy('tanggal', 'desc')->paginate(10),
      'pasar' => PasarModel::all(),
    );
    return view('Admin.Harga.index', $data, array('pasar' => $pasar));
  }
  public function addHarga(Request $request){
    $komoditi = array('komoditi' => KomoditiModel::all());
    $id_komoditi = $request->input('id_komoditi');
    $id_pasar = $request->input('id_pasar');
    $tanggal = $request->input('tanggal');
    $pasar = array('pasar' => PasarModel::all());
    if($request->isMethod('post')){
      $request->validate([
        'harga' => 'required'
      ]);
      $checkdata = DB::select(DB::raw("SELECT komoditi.nama_komoditi, pasar.nama_pasar, harga.tanggal FROM `harga`,`pasar`,`komoditi` WHERE harga.id_komoditi= '$id_komoditi' and harga.id_pasar= '$id_pasar' and harga.tanggal = '$tanggal' and harga.id_komoditi = komoditi.id and harga.id_pasar=pasar.id"));
      if ($checkdata!=null) {
        $_SESSION['duplicate_input']= "Data ".$checkdata[0]->nama_komoditi.", tanggal ".date("d-M-Y", strtotime($checkdata[0]->tanggal))." untuk ".$checkdata[0]->nama_pasar." sudah ada ";
      } else {
        $data = array(
          'id_komoditi' => $request->input('id_komoditi'),
          'id_pasar' => $request->input('id_pasar'),
          'tanggal' => $request->input('tanggal'),
          'harga' => $request->input('harga'),
          'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
          'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
        );
        try {
          $add = HargaModel::insert($data);
          $_SESSION['succes_input']= "Data Harga Berhasil Ditambahkan";
        } catch (\Exception $e) {
          $_SESSION['failed_input']= "Gagal Menambahkan Data Harga, Periksa Kembali Input Data Anda";
        }
      }
    }
    return view('Admin.Harga.add', $komoditi, $pasar);
  }
  public function editHarga($id, Request $request){
    $harga = HargaModel::find($id);
    $komoditi = KomoditiModel::all();
    $pasar = PasarModel::all();
    if($request->isMethod('post')){
      $request->validate([
        'harga' => 'required'
      ]);
      $harga->id_komoditi=$request->input('id_komoditi');
      $harga->id_pasar=$request->input('id_pasar');
      $harga->tanggal=$request->input('tanggal');
      $harga->harga=$request->input('harga');
      $harga->updated_at= \Carbon\Carbon::now()->toDateTimeString();
      try {
        $edit = $harga->save();
         $_SESSION['succes_input']= "Data harga berhasil diperbarui";
      } catch (\Exception $e) {
        $_SESSION['failed_input']= "Gagal memperbarui data harga, Periksa Kembali Input Data Anda";
      }
    }
    return view('Admin.Harga.edit', array('harga' => $harga, 'komoditi' => $komoditi, 'pasar' => $pasar));
  }
  public function deleteHarga($id){
    $harga = HargaModel::find($id);
    try {
      $delete = $harga->delete();
      echo "<script>alert('Data Berhasil Dihapus')</script>";
      echo '<meta http-equiv="refresh" content="0;url='.url('admin/harga').'">';
    } catch (\Exception $e) {
      echo "<script>alert('Gagal menghapus data harga')</script>";
      echo '<meta http-equiv="refresh" content="0;url='.url('admin/harga').'">';
    }
  }
  public function uploaded(){
    $year=DB::select(DB::raw("select distinct(year(tanggal)) as tahun from harga group by tanggal order by tanggal desc"));
    if(app('request')->get('month')=='' && app('request')->get('year')==''){
        $uploaded = DB::select(DB::raw("select tanggal,count(distinct(id_komoditi)) as jumlahkomoditi,count(id_komoditi) as jumlahdata from harga group by tanggal order by tanggal desc"));
    }elseif(app('request')->get('month')=='' && app('request')->get('year')!='') {
      $y=app('request')->get('year');
        $uploaded = DB::select(DB::raw("select tanggal,count(distinct(id_komoditi)) as jumlahkomoditi,count(id_komoditi) as jumlahdata from harga where year(tanggal)='$y' group by tanggal order by tanggal desc limit 50"));
    }else{
      $m=app('request')->get('month');
      $y=app('request')->get('year');
        $uploaded = DB::select(DB::raw("select tanggal,count(distinct(id_komoditi)) as jumlahkomoditi,count(id_komoditi) as jumlahdata from harga where month(tanggal)='$m' and year(tanggal)='$y' group by tanggal order by tanggal desc"));
    }
    return view('Admin.Uploaded.index', array('harga' => $uploaded,'year'=>$year));
  }
  public function perdate($tanggal){
    $data=HargaModel::where('tanggal', $tanggal )->with('pasar')->paginate(41);
    $pasar=PasarModel::all();
    return view('Admin.Uploaded.perdate',array('harga'=>$data,'pasar'=>$pasar, 'tanggal'=>$tanggal));
  }
  public function deleteperdate($tanggal){
    try {
      DB::select(DB::raw("delete from harga where tanggal ='$tanggal'"));
      echo "<script>alert('Data Berhasil Dihapus')</script>";
      echo '<meta http-equiv="refresh" content="0;url='.url('admin/harga/uploaded').'">';
    } catch (\Exception $e) {
      $_SESSION['failed_input']= "Gagal menghapus data!";
    }

  }
}
