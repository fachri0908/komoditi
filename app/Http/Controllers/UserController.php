<?php

namespace App\Http\Controllers;
use App\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
  public function index() {
    $data = array('user' => UserModel::all());
    return view('Admin.User.index', $data);
  }
  public function addUser(Request $request){
    if($request->isMethod('post')){
      $this->validate($request, [
        'username' => 'required',
        'password' => 'min:5'
      ]);
      $password = $request->input('password');
      $data = array(
        'username' => $request->input('username'),
        'password' => bcrypt($password),
        'remember_token' =>str_random(60)
      );
      try {
        $add = UserModel::insert($data);
        $_SESSION['succes_input']= "User berhasil ditambahkan";
      } catch (\Exception $e) {
        $_SESSION['failed_input']= "Gagal menambahkan user, periksa kembali inputan anda";
      }
    }
    return view('Admin.User.add');
  }
  public function editUser(Request $request){
    $user = UserModel::find(Auth::user()->id);
    $pass = bcrypt($request->input('password'));
    if($request->isMethod('post')){
      $this->validate($request, [
        'username' => 'required',
        'password' => 'min:5'
      ]);
      $user->username=$request->input('username');
      $user->password=$pass;
      try {
        $edit = $user->save();
        $_SESSION['succes_input']= "Data User berhasil diperbaharui";
      } catch (\Exception $e) {
        $_SESSION['failed_input']= "Gagal memperbarui data user, periksa kembali input anda";
      }
    }
    return view('Admin.User.edit', array('user' => $user));
  }
  public function deleteUser($id){
    if (Auth::user()->username!='dajoe') {
      return back();
    }
    $user = UserModel::find($id);
    $delete = $user->delete();
    if($delete){
      echo "<script>alert('Data Berhasil Dihapus')</script>";
      echo '<meta http-equiv="refresh" content="0;url='.url('admin/user').'">';
    }
  }
}
