<?php

namespace App\Http\Controllers;
use App\PasarModel;
use Illuminate\Http\Request;

class PasarController extends Controller
{
    public function index() {
      $data = array('pasar' => PasarModel::all());
      return view('Admin.Pasar.index', $data);
    }
    public function addPasar(Request $request){
      if($request->isMethod('post')){
        $request->validate([
          'nama_pasar' => 'required'
        ]);
        $data = array(
          'nama_pasar' => $request->input('nama_pasar')
        );
        try {
          PasarModel::insert($data);
          $_SESSION['succes_input']= "Data Pasar Berhasil Ditambahkan";
        } catch (\Exception $e) {
          $_SESSION['failed_input']= "Gagal Menambahkan Data Pasar, Periksa Kembali Input Data Anda";
        }
      }
      return view('Admin.Pasar.add');
    }
    public function editPasar($id, Request $request){
      $pasar = PasarModel::find($id);
      if($request->isMethod('post')){
        $request->validate([
          'nama_pasar' => 'required'
        ]);
        $pasar->nama_pasar=$request->input('nama_pasar');
        try {
          $pasar->save();
          $_SESSION['succes_input']= "Data Pasar Berhasil Diperbaharui";
        } catch (\Exception $e) {
          $_SESSION['failed_input']= "Gagal Memperbaharui Data Pasar, Periksa Kembali Input Data Anda";
        }
      }
      return view('Admin.Pasar.edit', array('pasar' => $pasar));
    }
    public function deletePasar($id){
      $pasar = PasarModel::find($id);
      try {
        $pasar->delete();
        return redirect('admin/pasar');
      } catch (\Exception $e) {
        echo "<script>alert('Gagal menghapus Data')</script>";
        echo '<meta http-equiv="refresh" content="0;url='.url('admin/pasar').'">';
      }
    }
}
?>
