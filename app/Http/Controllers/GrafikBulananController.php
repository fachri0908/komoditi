<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HargaModel;
use App\KomoditiModel;
use App\PasarModel;
use DB;
function getmonthname($d){
  switch ($d) {
    case '1':return 'Januari';break;
    case '2':return 'Februari';break;
    case '3':return 'Maret';break;
    case '4':return 'April';break;
    case '5':return 'Mai';break;
    case '6':return 'Juni';break;
    case '7':return 'Juli';break;
    case '8':return 'Agustus';break;
    case '9':return 'September';break;
    case '10':return 'Oktober';break;
    case '11':return 'November';break;
    case '12':return 'Desember';break;
  }
}
class GrafikBulananController extends Controller
{

  public function getchart(){
    $y=app('request')->input('y');
    if(app('request')->input('id_komoditi') == ''){
      $komoditi = KomoditiModel::limit(1)->get();
      $idkomoditi = $komoditi[0]->id;
    }else {
      $idkomoditi = app('request')->input('id_komoditi');
      $komoditi = KomoditiModel::where('id', app('request')->input('id_komoditi'))->get();
    }
    $lastdate = HargaModel::groupBy('tanggal')->orderBy('tanggal' ,'desc')->limit(2)->get();
    if(app('request')->input('m1') == ''){
      $m1 = $lastdate[0]->tanggal;
    }else {
      $m1 = app('request')->input('m1');
    }
    if(app('request')->input('m2') == ''){
      $m2 = $lastdate[1]->tanggal;
    }else {
      $m2 = app('request')->input('m2');
    }
    $harga = DB::select(DB::raw("select komoditi.nama_komoditi,hargaa.id_komoditi,
    month(hargaa.tanggal) as bulan, avg(hargaa.harga) as pasarraya, avg(hargab.harga) as pasarnanggalo,
    avg(hargac.harga) as pasarlubukbuaya from harga as hargaa, harga as hargab , harga as hargac, komoditi
    where hargaa.id_komoditi = hargab.id_komoditi and hargaa.id_komoditi = hargac.id_komoditi
    and year(hargaa.tanggal) = '$y' and year(hargab.tanggal) = '$y' and year(hargac.tanggal) = '$y' and hargaa.id_pasar =1
    and hargab.id_pasar=2 and hargac.id_pasar=3 AND komoditi.id=hargaa.id_komoditi and (month(hargaa.tanggal) between '$m2' and '$m1')
    and hargaa.id_komoditi='$idkomoditi' group by month(hargaa.tanggal) ORDER BY `hargaa`.`tanggal` DESC"));

      $result2[]= ['Tanggal','Pasar Raya', 'Pasar Nanggalo', 'Pasar Lubuk Buaya'];
      foreach ($harga as $key => $value) {
          $result2[++$key] = [getmonthname($value->bulan), (int)$value->pasarraya, (int)$value->pasarnanggalo, (int)$value->pasarlubukbuaya];
      }
      return view('WebView.grafikbulanan')
              ->with('harga',json_encode($result2))
              ->with('komoditi',json_encode($komoditi[0]->nama_komoditi))
              ->with('satuan',json_encode($komoditi[0]->satuan->nama_satuan))
              ->with('listkomoditi', KomoditiModel::orderBy('nama_komoditi')->get())
              ->with('listtahun', DB::select(DB::raw("select year(tanggal) as tahun from harga group by year(tanggal) order by tanggal desc")))
              ->with('latestdate', $m2)
              ->with('previousdate', $m1);
  }
}
