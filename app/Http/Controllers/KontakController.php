<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KontakModel;

class KontakController extends Controller
{
    public function add(Request $request){
      if($request->isMethod('post')){
        $request->validate([
          'email' => 'required|email',
          'pesan' => 'required'
        ]);
        $data = array(
          'nama' => $request->input('nama'),
          'email' => $request->input('email'),
          'pesan' => $request->input('pesan'),
          'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
          'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
        );
        try {
          KontakModel::insert($data);
          $_SESSION['succes_input']= "Pesan anda telah kami terima";
        } catch (\Exception $e) {
          $_SESSION['failed_input']= "Gagal mengirim pesan, periksa kembali input data anda";
        }
      }
      return view('WebView.kontak');
    }
    public function index(){
      $kontak=KontakModel::all();
      return view('Admin.Kontak.index',array('kontak'=>$kontak));
    }
    public function delete($id){
      $kontak = KontakModel::find($id);
      try {
        $kontak->delete();
        return redirect('admin/kontak');
      } catch (\Exception $e) {
        echo "<script>alert('Gagal menghapus Data')</script>";
        echo '<meta http-equiv="refresh" content="0;url='.url('admin/kontak').'">';
      }
    }
}
