<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KomoditiModel;
use App\PasarModel;
use App\HargaModel;
use DB;
class ViewController extends Controller
{
  public function index(){
    $lastdate = HargaModel::groupBy('tanggal')->orderBy('tanggal' ,'desc')->limit(2)->get();
    $latestdate = $lastdate[0]->tanggal;
    $previousdate = $lastdate[1]->tanggal;
    $data = array(
      'pasar' => PasarModel::all(),
      'komoditi' => KomoditiModel::all(),
      'harga' => DB::select(DB::raw("SELECT harga.id,harga.tanggal, harga.harga, harga.id_komoditi, harga.id_pasar,
        komoditi.nama_komoditi, pasar.nama_pasar, satuan.nama_satuan FROM `harga`,`komoditi`,`pasar`,`satuan`
        where harga.tanggal = '$latestdate' and harga.id_komoditi = komoditi.id and harga.id_pasar = pasar.id
        and satuan.id=komoditi.id_satuan order by harga.id_pasar, harga.id_komoditi")),
      'harga2' => DB::select(DB::raw("SELECT harga.harga FROM `harga`,`komoditi`,`pasar`,`satuan`
        where harga.tanggal = '$previousdate' and harga.id_komoditi = komoditi.id and harga.id_pasar = pasar.id
        and satuan.id=komoditi.id_satuan order by harga.id_pasar, harga.id_komoditi")),
    );
    return view('WebView.index', $data, array('lastdate' => $lastdate));
  }
  public function indextry(){
    $p=PasarModel::all();
    $p1date= HargaModel::select('tanggal')->where('id_pasar',$p[0]->id)->groupBy('tanggal')->orderBy('tanggal' ,'desc')->limit(2)->get();
    $p2date= HargaModel::select('tanggal')->where('id_pasar',$p[1]->id)->groupBy('tanggal')->orderBy('tanggal' ,'desc')->limit(2)->get();
    $p3date= HargaModel::select('tanggal')->where('id_pasar',$p[2]->id)->groupBy('tanggal')->orderBy('tanggal' ,'desc')->limit(2)->get();
    $p1id=$p[0]->id;$p1latestdate=$p1date[0]->tanggal;$p1previousdate=$p1date[1]->tanggal; //p1
    $p2id=$p[1]->id;$p2latestdate=$p2date[0]->tanggal;$p2previousdate=$p2date[1]->tanggal; //p2
    $p3id=$p[2]->id;$p3latestdate=$p3date[0]->tanggal;$p3previousdate=$p3date[1]->tanggal; //p3
    $p1data=array(
      'p1latestdate'=>$p1latestdate, 'p1previousdate'=>$p1previousdate,
      'p2latestdate'=>$p2latestdate, 'p2previousdate'=>$p2previousdate,
      'p3latestdate'=>$p3latestdate, 'p3previousdate'=>$p3previousdate,
      'p1name'=>$p[0]->nama_pasar,
      'p2name'=>$p[1]->nama_pasar,
      'p3name'=>$p[2]->nama_pasar,
      'p1harga'=>DB::select(DB::raw("SELECT h.id_komoditi,h.harga,h.tanggal, h1.harga as hargasebelumnya, h1.tanggal as tglsebelumnya,
        k.nama_komoditi from komoditi k, harga h inner join harga h1 on h.id_komoditi=h1.id_komoditi and h.id_pasar=h1.id_pasar
        and h1.tanggal='$p1previousdate' where h.tanggal='$p1latestdate' and h.id_pasar='$p1id' and k.id=h.id_komoditi")),
      'p2harga'=>DB::select(DB::raw("SELECT h.id_komoditi,h.harga,h.tanggal, h1.harga as hargasebelumnya, h1.tanggal as tglsebelumnya,
        k.nama_komoditi from komoditi k, harga h inner join harga h1 on h.id_komoditi=h1.id_komoditi and h.id_pasar=h1.id_pasar
        and h1.tanggal='$p2previousdate' where h.tanggal='$p2latestdate' and h.id_pasar='$p2id' and k.id=h.id_komoditi")),
      'p3harga'=>DB::select(DB::raw("SELECT h.id_komoditi,h.harga,h.tanggal, h1.harga as hargasebelumnya, h1.tanggal as tglsebelumnya,
        k.nama_komoditi from komoditi k, harga h inner join harga h1 on h.id_komoditi=h1.id_komoditi and h.id_pasar=h1.id_pasar
        and h1.tanggal='$p3previousdate' where h.tanggal='$p3latestdate' and h.id_pasar='$p3id' and k.id=h.id_komoditi")),
    );
    return view('WebView.index2', $p1data);
  }
  public function test(){
    return view('WebView.layout');
  }

}
