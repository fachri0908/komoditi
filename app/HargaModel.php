<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\KomoditiModel;
use App\PasarModel;
class HargaModel extends Model
{
  protected $table = "harga";
  public function komoditi() {
    return $this ->belongsTo(KomoditiModel::class, 'id_komoditi');
  }
  public function pasar() {
    return $this ->belongsTo(PasarModel::class, 'id_pasar');
  }
}
