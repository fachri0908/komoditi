<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\KategoriModel;
use App\SatuanModel;
class KomoditiModel extends Model
{
  protected $table = "komoditi";
  public $timestamps = false;
  public function kategori() {
    return $this->belongsTo(KategoriModel::class, 'id_kategori');
  }
  public function satuan() {
    return $this->belongsTo(SatuanModel::class, 'id_satuan');
  }
}
