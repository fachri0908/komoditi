<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SatuanModel extends Model
{
  protected $table = "satuan";
  public $timestamps = false;
}
