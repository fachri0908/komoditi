<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasarModel extends Model
{
    protected $table = "pasar";
    public $timestamps = false;
}
